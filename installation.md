
# Installation 'legacy'
## Préparation du systeme

mettre à jour le système
```bash
dnf upgrade -y
```

Ouverture du pare feux 
```bash
dnf install firewalld
systemctl enable firewalld
systemctl start firewalld
```

```bash
firewall-cmd --permanent --zone=public --add-port=8080/tcp
firewall-cmd --reload
```

## Installation et préparation de la base de donnée

Installer la base de données
```bash
dnf install -y mysql-server
```

Avticer le service
```bash
systemctl enable mysqld
systemctl start mysqld
```

Installation minimal de mysql
```bash
mysql_secure_installation
```

création de la base et du user sql
```bash
mysql -u root -e "
CREATE USER 'querimonia'@'localhost' IDENTIFIED BY 'password'; \
CREATE DATABASE IF NOT EXISTS querimonia CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci; \
GRANT ALL PRIVILEGES ON querimonia.* TO 'querimonia'@'localhost'; \
FLUSH PRIVILEGES;"
```

création des tables et peuplement de base:
```bash
mysql -u root -D querimonia < init.sql
mysql -u root -D querimonia < populate.sql
```

## Installation de querimonia

installation du jre java
```bash
dnf install -y java-17-openjdk-headless
```

création d'un utilisateur dédié
```bash
adduser --system --no-create-home --user-group querimonia
```

création du repertoire
```bash
mkdir -p /opt/querimonia
chmod 700 /opt/querimonia
```

télécharger le fichier et le placer dans le bon repertoire (`/opt/querimonia`)

donner les bonnes permissions
```bash
chmod 500 /opt/querimonia/querimonia.jar
chown -R querimonia:querimonia /opt/querimonia
```

crere un service systemd
```bash
cat > /etc/systemd/system/querimonia.service <<EOF
[Unit]
Description=Demon de l'appication Querimonia
ConditionFileIsExecutable=/opt/querimonia/querimonia.jar
After=network.target

[Service]
Type=simple
Environment=SERVER_PORT=8080
Environment=DATABASE_SYSTEM=mysql
Environment=DATABASE_ADDRESS=localhost
Environment=DATABASE_PORT=3306
Environment=DATABASE_DATABASE=XXX
Environment=DATABASE_USER=XXX
Environment=DATABASE_PASSWORD=XXX
Environment=ADMIN_USER=XXX
Environment=ADMIN_PASSWORD=XXX
Environment=JWT_SECRET=XXX
User=querimonia
Group=querimonia
SuccessExitStatus=143
WorkingDirectory=/opt/querimonia
ExecStart=/bin/java -jar /opt/querimonia/querimonia.jar

[Install]
WantedBy=multi-user.target
EOF
```
