# insertion des classes
INSERT INTO classe (anne_propotion, id_classe, groupe, libelle, parcourt, sousgroupe)
VALUES
(2023, UNHEX(REPLACE(UUID(), '-', '')), '1', ' Groupe 1-A', '', 'A')
     , (2023, UNHEX(REPLACE(UUID(), '-', '')), '1', ' Groupe 1-B', '', 'B')
     , (2023, UNHEX(REPLACE(UUID(), '-', '')), '2', ' Groupe 2-A', '', 'A')
     , (2023, UNHEX(REPLACE(UUID(), '-', '')), '2', ' Groupe 2-B', '', 'B')
     , (2023, UNHEX(REPLACE(UUID(), '-', '')), '3', ' Groupe 3-A', '', 'A')
     , (2023, UNHEX(REPLACE(UUID(), '-', '')), '3', ' Groupe 3-B', '', 'B')
     , (2023, UNHEX(REPLACE(UUID(), '-', '')), '4', ' Groupe 4-A', '', 'A')
     , (2023, UNHEX(REPLACE(UUID(), '-', '')), '4', ' Groupe 4-B', '', 'B')
     , (2023, UNHEX(REPLACE(UUID(), '-', '')), '5', ' Groupe 5-A', '', 'A')
     , (2023, UNHEX(REPLACE(UUID(), '-', '')), '5', ' Groupe 5-B', '', 'B');

# classification des informations
INSERT INTO classification (id_classification, libelle, description)
VALUES
    (UNHEX(REPLACE(UUID(), '-', '')), 'Info', 'Information ni urgente ni officielle, traitez là avec soin.'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'Annonce officielle', 'Ceci est une annonce officielle.'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'Annonce de professeur', 'Ceci est une annonce de l\'un des proffesseur.'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'Urgent', 'Ceci est une information urgente.');

INSERT INTO matiere (id_matiere, code, libelle)
VALUES
    (UNHEX(REPLACE(UUID(), '-', '')), 'R1.01', 'Initiation au développement'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R1.11', 'Communication S1'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'SAE-1.06', 'Découverte de l\'environnement économique et écologique'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'SAE-1.04', 'Création d\'une base de données'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'SAE-1.03', 'Installation d\'un poste pour le développement'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'S1.02', 'Comparaison d\'approches algorithmiques'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'S1.01', 'Implémentation d\'un besoin client'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R1.12', 'Projet professionnel et personnel'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R1.10', 'ANGLAIS'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R1.09', 'Economie durable et numérique'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R1.07', 'Outils mathématiques fondamentaux'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R1.06', 'Mathématiques discrètes'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R1.05', 'Introduction aux bases de données et SQL'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R1.08', 'Introduction aux bases de données et SQL'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R1.04', 'Introduction aux systèmes d\'exploitation et à leur fonctionnement'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R1.03', 'Introduction à l\'architecture des ordinateurs'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R1.02', 'Développement d\'interfaces web');

INSERT INTO matiere (id_matiere, code, libelle)
VALUES
    (UNHEX(REPLACE(UUID(), '-', '')), 'SAE-2.04', 'Exploitation d\'une base de données'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'SAE-2.03', 'Installation de services réseau'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'SAE-2.02', 'Exploration algorithmique d\'un problème'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R2.12', 'ANGLAIS'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R2.11', 'Droit des contrats et du numérique'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R2.02', 'Développement d\'applications avec IHM'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R2.09', 'Méthodes numériques'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R2.07', 'Graphes'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R2.05', 'Introduction aux services réseaux'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R2.04', 'Communication et fonctionnement bas niveau'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R2.06', 'Exploitation d\'une base de données'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R2.14', 'Projet professionnel et personnel : métiers de l\'informatique');


INSERT INTO matiere (id_matiere, code, libelle)
VALUES
    (UNHEX(REPLACE(UUID(), '-', '')), 'R3.09', 'Cryptographie et sécurité'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R3.14', 'PROJET PERSONNEL ET PROFESSIONNEL'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'SAE-3A.01', 'SAé -Lambdas et streams en Java'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'SAE-3A.02', 'SAé -Patron MVC - Application en Java'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R3.05', 'Programmation système'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R3.04', 'Qualité de Développement (Patrons, SOLID, Refactoring)'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R3.10', 'Management des systèmes d\'information'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R3.13', 'Communication professionnelle'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R3.03', 'Analyse UML'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R3.11', 'Droit du numérique'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R3.06', 'Architecture des réseaux'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'SAE-3B.01', 'Déploiement d\'applications communicantes et sécurisées'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'SAE-3C.01', 'Création et exploitation d’une base de données'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R3.07', 'SQL dans un langage de programmation'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R3.02', 'Dév. Efficace : Algos & Structure de données');

INSERT INTO matiere (id_matiere, code, libelle)
VALUES
    (UNHEX(REPLACE(UUID(), '-', '')), 'R4B10', 'Cryptographie et sécurité'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R4C08', 'Cryptographie et sécurité'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R4.A.12', 'Automates et langages'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R4.A.11', 'Développement Natif Android_matiere'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R4.01', 'Architecture Logicielle'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R4.02', 'Qualité de développement (production de tests, JUnit5, doublures de tests)'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R4A.09', 'Management avancé des SI'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R4B.09', 'Management avancé des SI'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R4C.11', 'Management avancé des SI STK'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'SAE-4C.01', 'Développement avec une base de données et visualisation'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R4.03', 'Qualité et au-delà du relationnel'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R4.04', 'Méthodes d\'optimisation'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R4.07', 'PROJET PERSONNEL ET PROFESSIONNEL'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R4A.08', 'Virtualisation'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R4B.08', 'Virtualisation'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R4B.11', 'Réseau avancé'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R4C.09', 'Réseau avancé'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R4B.12', 'Sécurité système et réseaux');

INSERT INTO matiere (id_matiere, code, libelle)
VALUES
    (UNHEX(REPLACE(UUID(), '-', '')), 'SAE-5A', 'SAE du parcours A'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R5.A14', 'Anglais'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R5.C12', 'Anglais'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R5.B12', 'Anglais'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R5.A08', 'Gestion de projet et développement'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R5.A10', 'Nouvelles Bases de données'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R5.03', 'Communication S5'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'SAE-5C.01', 'Proposer une solution optimisée à partir de données'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R5.C.04', 'Programmation au format web des informations décisionnelles'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R5.C.05', 'Nouvelles Bases de données'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R5.AGED.08', 'Techniques d’intelligence artificielle Informatique'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'SAE-5B', 'Evolution d\'infrastructure'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R5.A13', 'Economie durable et numérique'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R5.B11', 'Economie durable et numérique'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R5.C10', 'Economie durable et numérique'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R5.A09', 'Virtualisation Avancée'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R5.B07', 'Virtualisation Avancée'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R5.AGED.06', 'Exploitation de bases de données'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'SAE-5B.02', 'Evolution d’une infrastructure'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'R5.B.09', 'Cybersécurité');

# les TAGS
# un par matière
INSERT INTO tag (id_tag, libelle, description)
SELECT UNHEX(REPLACE(UUID(), '-', '')), code, libelle
FROM matiere;

# autre
INSERT INTO tag (id_tag, libelle, description)
VALUES
    (UNHEX(REPLACE(UUID(), '-', '')), 'Autre', 'Si vous pensez que ça mérite un tag !');

# oui pleins de trucs
INSERT INTO tag (id_tag, libelle, description)
VALUES
    (UNHEX(REPLACE(UUID(), '-', '')), 'Emplois du temps', 'Pour les info EDT vérifiés'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'Événements', 'Événements'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'Inscriptions', 'Inscriptions'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'Administration', 'Administration'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'Projet', 'Projet'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'Stage', 'Stage'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'Alternance', 'Alternance'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'Orientation', 'Orientation'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'Vie étudiante', 'Vie étudiante'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'Actualités technologiques', 'Actualités technologiques'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'Annonce officielle', 'Annonce officielle'),
    (UNHEX(REPLACE(UUID(), '-', '')), 'Retours querimonia', 'Tag pour faire vos retours sur querimonia !'),
# les groupes
    (UNHEX(REPLACE(UUID(), '-', '')), '1A', 'Groupe 1A'),
    (UNHEX(REPLACE(UUID(), '-', '')), '1B', 'Groupe 1B'),
    (UNHEX(REPLACE(UUID(), '-', '')), '1C', 'Groupe 1C'),
    (UNHEX(REPLACE(UUID(), '-', '')), '1D', 'Groupe 1D'),
    (UNHEX(REPLACE(UUID(), '-', '')), '1E', 'Groupe 1E'),
    (UNHEX(REPLACE(UUID(), '-', '')), '1F', 'Groupe 1F'),
    (UNHEX(REPLACE(UUID(), '-', '')), '2A', 'Groupe 2A'),
    (UNHEX(REPLACE(UUID(), '-', '')), '2B', 'Groupe 2B'),
    (UNHEX(REPLACE(UUID(), '-', '')), '2C', 'Groupe 2C'),
    (UNHEX(REPLACE(UUID(), '-', '')), '2D', 'Groupe 2D'),
    (UNHEX(REPLACE(UUID(), '-', '')), '2E', 'Groupe 2E'),
    (UNHEX(REPLACE(UUID(), '-', '')), '2F', 'Groupe 2F'),
    (UNHEX(REPLACE(UUID(), '-', '')), '3A', 'Groupe 3A'),
    (UNHEX(REPLACE(UUID(), '-', '')), '3B', 'Groupe 3B'),
    (UNHEX(REPLACE(UUID(), '-', '')), '3C', 'Groupe 3C'),
    (UNHEX(REPLACE(UUID(), '-', '')), '3D', 'Groupe 3D'),
    (UNHEX(REPLACE(UUID(), '-', '')), '3E', 'Groupe 3E'),
    (UNHEX(REPLACE(UUID(), '-', '')), '3F', 'Groupe 3F');


