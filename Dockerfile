FROM  ubuntu:22.04
MAINTAINER Tcp
RUN apt-get update && apt-get install -y openjdk-17-jdk-headless
COPY ./target/querimonia-0.0.1-SNAPSHOT.jar ./querimonia.jar
ENV PATH=$PATH:/usr/bin/java
ENTRYPOINT ["java"]
CMD ["-jar","./querimonia.jar"]
