# Querimonia

Notre projet consiste en la création d'une API qui permet de mettre en place un forum interactif en ligne. Son comportement s'apparente à celui d'un fil d'actualité avec des messages.

Cette plateforme offre aux utilisateurs la possibilité de partager des messages, d'interagir les uns avec les autres, et de créer une communauté autour de sujets variés. 

Voici un aperçu des principales fonctionnalités de notre API :

- **publication de messages** : les utilisateurs peuvent créer et publier des messages sur différents sujets. Ces messages peuvent contenir du texte, des médias, et même des tags pour faciliter la recherche et l'organisation des discussions.

- **système de tags** : notre API propose un système de tags inspiré des # que l'on trouve sur les réseaux sociaux. Les utilisateurs peuvent associer des tags à leurs messages pour les catégoriser et les rendre plus facilement accessibles à d'autres utilisateurs intéressés par les mêmes sujets.

- **réponses aux messages** : les membres de la communauté peuvent interagir en répondant aux messages, ce qui favorise les discussions et les échanges d'idées.

- **classification des messages** : chaque message peut être classé en fonction de son importance, ce qui permet de distinguer les messages essentiels des messages moins prioritaires. Cette classification aide les utilisateurs à se concentrer sur les sujets qui leur importent le plus.

- **statuts d'utilisateur** : Notre API prend en charge différents statuts pour les utilisateurs, y compris élève, professeur, et gérant/administrateur. Chacun de ces statuts offre des privilèges et des responsabilités spécifiques au sein de la communauté.

- **recherche avancée** : Les utilisateurs peuvent effectuer des recherches avancées pour trouver des messages en fonction de critères tels que les tags, l'auteur du message, la date de publication, et bien plus encore.

## Règlement du concours
<details>
<summary>wesh</summary>

### Article 1 : Objectif du Défi
1.1 Le présent défi a pour but de susciter une réflexion concrète sur un sujet choisi par
l’association « Toulouse Club Programmation ». ». Il s’agit pour les participants de concevoir et
développer une interface conviviale qui interagit avec l'API. L'objectif pour les étudiants est de
savoir mettre en valeur leur réalisation, d'argumenter sur son intérêt, et de prendre du recul.
### Article 2 : Participants et durée du défi
2.1 Le concours est ouvert aux étudiants scolarisés à l’IUT Paul Sabatier de Toulouse au
département informatique. La participation au concours est gratuite et ne vous engage en
rien.
2.2 Ce concours de programmation se déroule du DATE DU CONCOURS(date de début du
concours) au DATE DU CONCOURS (remise du/des prix). Les participants auront la possibilité
de réaliser leur programme à leur domicile.
### Article 3 : Langages et Outils
3.1 Le programme sera réalisé par les élèves eux-mêmes. Les participants sont libres d’utiliser
tous les logiciels et langages de programmation dont ils disposent pour leur création de
programme. Aucune dépenses liées au concours ne seront pas remboursés par l’IUT et
l’association.
### Article 4 : Soumission des Projets
4.1 Les participants doivent soumettre leur projet sur une plateforme de gestion de code
source GIT, à savoir GitHub ou GitLab. Le projet doit être hébergé publiquement pour que les
juges puissent y accéder. Les participants doivent inclure un fichier README dans leur dépôt
GitHub ou GitLab. Ce fichier doit contenir un lien URL vers l'interface graphique fonctionnelle.
4.2 Nous déclinons toute responsabilité en cas de non-reçus de votre participation
(notamment en raison de problèmes de connectivités web). En cas de litige à propos d’un
rendu en retard, l’heure de réception du programme sur le serveur discord primera.
### Article 5 : Date Limite de Soumission
5.1 La date limite de soumission des projets est fixée au [date de clôture du défi] à [heure de
clôture du défi], heure locale.
5.2 Tout rendu en retard du programme équivaudra à une disqualification de l’équipe, sauf en
cas de problème découlant de notre responsabilité.
### Article 6 : Responsabilité des Participants
6.1 Les étudiants sont responsables en tout point de leur oeuvre. Tout plagiat ou utilisation
non autorisée de ressources tierces entraînera la disqualification.
### Article 7 : Évaluation et Résultats
7.1 Les projets soumis seront évalués en fonction de critères prédéfinis par le jury. Les
résultats seront annoncés sur le serveur Discord de l'association "Toulouse Club
Programmation" à partir du [date d'annonce des résultats].
### Article 8 : Récompenses
8.1 Les projets gagnants recevront LEUR INTERFACE SERA UTILISE
### Article 9 : Organisation du concours
9.1 Les membres du TCP, responsable et chargé de l’organisation du concours et de son bon
déroulement se réservent le droit de faire évoluer les dates des concours. L’organisation de cet
événement est soumise à l’évolution de la situation sanitaire. Toutes les modifications seront
communiquées aux participants via le serveur Discord de l'association.
### Article 10 : Partage des oeuvres
10.1 Il est expressément convenu qu’en participant au présent concours, les participants et
lauréats autorisent l’organisation du concours à partager leurs oeuvres sur le serveur discord
de l’association.
### Article 11 : Réglementation légale
11.1 Les participants sont soumis à la réglementation française applicable aux jeux et
concours. La participation à ce concours implique l’acceptation pleine et entière sans
restriction ni réserve du présent règlement, sans possibilité de réclamation contre les
résultats.
### Article 12 : Informations Complémentaires
12.1 Le présent règlement, la grille d’évaluation des programmes ainsi que le sujet du
concours seront consultables sur le serveur de l’association discord de l’association.
### Article 13 : Droits d’auteur
13.1 Le droit d'auteur est régi par le Code de la Propriété Intellectuelle (CPI) du 1er juillet
1992 qui regroupe les lois relatives à la propriété intellectuelle, notamment la loi du 11 mars
1957 et celle du 3 juillet 1985. De ce fait, les participants ne sont autorisés à publier
uniquement des programmes qu’ils ont réalisés par leur propres moyens matériels et
intellectuels.
### Article 14 : Composition des équipes
14.1 Les équipes seront composées de 1 personnes minimum et 3 personnes maximum. Il n’y
a pas de règle de mixité quant à la composition des équipes. Tout étudiant ne peut être inscrit
que dans une et une seule équipe.
### Article 15 : Participants mineurs
15.1 Si un participant mineur souhaite s’inscrire et participer au concours, il devra remplir et
envoyer aux organisateurs du concours une autorisation parentale de participation au
concours.
### Article 16 : Acceptation des Règles
16.1. La participation à ce défi implique l'acceptation complète et sans réserve de ce
règlement. Les participants doivent respecter les règles et les délais fixés.
### Article 17 : Informations Complémentaires
17.1. Toutes les informations complémentaires, y compris le sujet du défi et les critères
d'évaluation, seront disponibles sur le serveur Discord de l'association.
Nous vous encourageons à respecter ce règlement et à participer activement au défi. Bonne
chance à tous les participants !
</details>
