package com.tcp.querimonia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuerimoniaApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuerimoniaApplication.class, args);
	}

}
