package ressources.message;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.NoArgsConstructor;

@Hidden
@NoArgsConstructor
public class
MessageNotFoundException extends RuntimeException{
    public static String message = "Le message n'existe pas";

    public MessageNotFoundException(String s) {
        super(s);
    }
}
