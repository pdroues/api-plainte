package ressources.message.tag;

import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Hidden
@Getter
@Setter
@Entity(name = "tag")
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id_tag")
    private UUID id;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "description")
    private String description;

}