package ressources.message;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class NewMessageDTO {
    private UUID idClassification;
    private String titre;
    private String texte;
    private List<UUID> tags;
}
