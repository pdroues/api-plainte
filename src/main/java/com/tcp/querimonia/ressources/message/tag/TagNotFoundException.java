package ressources.message.tag;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.NoArgsConstructor;

@Hidden
@NoArgsConstructor
public class TagNotFoundException extends RuntimeException{

    public TagNotFoundException(String s) {
        super(s);
    }
}
