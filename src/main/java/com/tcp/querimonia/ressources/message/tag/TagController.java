package ressources.message.tag;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/tag")
public class TagController {

    @Autowired
    private TagRepository repo;

    @ExceptionHandler({ TagNotFoundException.class })
    public ResponseEntity handleTagNotFoundException() {
        return new ResponseEntity("Le tag n'existe pas", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ TagAlreadyExistException.class })
    public ResponseEntity handleTagAlreadyExistException() {
        return new ResponseEntity("Un tag existe déjà sous ce nom", HttpStatus.NOT_FOUND);
    }

    @Operation(summary = "Récupère la liste de tous les tags.")
    @GetMapping()
    public List<Tag> getAll() {
        return repo.findAll();
    }

    @Operation(summary = "Crée un nouveau tag. (ADMIN ou PROF)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tag créé avec succès !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Tag.class)) }),
            @ApiResponse(responseCode = "409", description = "Le tag existe déjà", content = @Content),
            @ApiResponse(responseCode = "403", description = "Si vous n'avez pas le droit d'en créer", content = @Content)
    })
    @PostMapping
    public Tag createTag(@RequestBody Tag _toCreate) {
        if (repo.findAll().stream().anyMatch(tag -> tag.getLibelle().equals(_toCreate.getLibelle()))) {
            throw new TagAlreadyExistException();
        }
        return repo.save(_toCreate);
    }

    @Operation(summary = "Récupère un tag en fonction de son identifiant.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tag trouvé !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Tag.class)) }),
            @ApiResponse(responseCode = "400", description = "Identifiant invalide fourni", content = @Content),
            @ApiResponse(responseCode = "404", description = "Tag non trouvé", content = @Content)
    })
    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Tag getById(@PathVariable UUID id) {
        return repo.findById(id).orElseThrow(TagNotFoundException::new);
    }
}
