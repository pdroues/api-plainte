package ressources.message.clasification;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/classification")
public class ClassificationController {
    @Autowired
    private ClassificationRepository repo;

    @Operation(summary = "Récupère la liste de toutes les classifications.")
    @GetMapping()
    public List<Classification> getAll() {
        return repo.findAll();
    }

    @ExceptionHandler({ ClassificationNotFoundException.class })
    public ResponseEntity<String> handleClassificationNotFoundException() {
        return new ResponseEntity<>("La classification n'existe pas", HttpStatus.NOT_FOUND);
    }

    @Operation(summary = "Récupère une classification en fonction de son identifiant.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Classification trouvée !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Classification.class)) }),
            @ApiResponse(responseCode = "400", description = "Identifiant invalide fourni", content = @Content),
            @ApiResponse(responseCode = "404", description = "Classification non trouvée", content = @Content)
    })
    // Renvoie la classification en fonction de l'ID
    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Classification getById(@PathVariable UUID id) {
        return repo.findById(id).orElseThrow(ClassificationNotFoundException::new);
    }
}
