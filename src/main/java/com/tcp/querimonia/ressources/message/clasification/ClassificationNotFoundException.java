package ressources.message.clasification;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.NoArgsConstructor;

@Hidden
@NoArgsConstructor
public class ClassificationNotFoundException extends RuntimeException{

    public ClassificationNotFoundException(String s) {
        super(s);
    }
}
