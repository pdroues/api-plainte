package ressources.message.clasification;


import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Hidden
@Getter
@Setter
@Entity(name = "classification")
public class Classification {

    @Id
    @Column(name = "id_classification")
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;
    @Column(name = "libelle")
    private String libelle;
    @Column(name = "description")
    private String description;


}
