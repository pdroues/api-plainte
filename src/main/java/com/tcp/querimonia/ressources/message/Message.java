package ressources.message;

import ressources.humain.Humain;
import ressources.message.clasification.Classification;
import ressources.message.tag.Tag;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Hidden
@Data
@NoArgsConstructor
@Entity(name = "message")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id_message")
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "id_categorie")
    private Classification clasification;

    @Column(name = "date_publication", updatable = false)
    private LocalDateTime date_publication = LocalDateTime.now();

    @Column(name = "titre")
    private String titre;

    @Column(name = "texte")
    private String texte;

    @ManyToOne()
    @JoinColumn(name = "id_humain")
    private Humain auteur;

    @ManyToMany
    @JoinTable(
            name = "tagged",
            joinColumns = @JoinColumn(name = "id_message"),
            inverseJoinColumns = @JoinColumn(name = "id_tag")
    )
    private Set<Tag> tags;

    public Message(
            Classification clasification,
            String titre,
            String texte,
            Humain auteur,
            Set<Tag> tags
    ) {
        this.clasification = clasification;
        this.date_publication = LocalDateTime.now();
        this.titre = titre;
        this.texte = texte;
        this.auteur = auteur;
        this.tags = tags;
    }
}
