package ressources.message.clasification;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Hidden
@Repository public interface ClassificationRepository extends JpaRepository<Classification, UUID> {

}