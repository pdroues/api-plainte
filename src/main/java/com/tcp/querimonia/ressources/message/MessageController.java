package ressources.message;

import ressources.humain.HumainNotFoundException;
import ressources.humain.HumainRepository;
import ressources.message.clasification.ClassificationNotFoundException;
import ressources.message.clasification.ClassificationRepository;
import ressources.message.tag.TagRepository;
import ressources.user.UserRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/message")
public class MessageController {

    private MessageRepository repo;
    private TagRepository tagRepo;
    private HumainRepository humainRepo;
    private ClassificationRepository clasificationRepo;
    private UserRepository userRepo;

    @Autowired
    public MessageController(MessageRepository repo, TagRepository tagRepo, HumainRepository humainRepo, ClassificationRepository clasificationRepo, UserRepository userRepo) {
        this.repo = repo;
        this.tagRepo = tagRepo;
        this.humainRepo = humainRepo;
        this.clasificationRepo = clasificationRepo;
        this.userRepo = userRepo;
    }
    @ExceptionHandler({ MessageNotFoundException.class })
    public ResponseEntity<String> handleMessageNotFoundException() {
        return new ResponseEntity<>(MessageNotFoundException.message, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ ClassificationNotFoundException.class })
    public ResponseEntity<String> handleClassificationNotFoundException() {
        return new ResponseEntity<>("La classification n'existe pas", HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler({ HumainNotFoundException.class })
    public ResponseEntity<String> handleHumainNotFoundException() {
        return new ResponseEntity<>("La classification n'existe pas", HttpStatus.NOT_FOUND);
    }


    @Operation(summary = "Renvoie tous les messages")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Tous les messages renvoyés",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Message.class))
            })
    })
    @GetMapping()
    public List<Message> getAll() {
        return repo.findAll();
    }

    @Operation(summary = "Renvoie un message en fonction d'un id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Message trouvé !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Message.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Message pas trouvé",
                    content = @Content) })
    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Message getById(@PathVariable UUID id) {
        return repo.findById(id).orElseThrow(MessageNotFoundException::new);
    }

    @Operation(summary = "Renvoie une liste de message en fonction d'un tag")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Message(s) trouvé(s) !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Message.class)) }),
            @ApiResponse(responseCode = "404", description = "Invalid Tag id supplied",
                    content = @Content) })
    @RequestMapping(path = "/tag/{tagId}", method = RequestMethod.GET)
    public List<Message> getByTag(@PathVariable UUID tagId) {
        return repo.findAll().stream()
                .filter(message -> message.getTags().stream().anyMatch(tag -> tag.getId().equals(tagId)))
                .collect(Collectors.toList());
    }

    @Operation(summary = "Renvoie une liste de message en fonction de qui l'a écrit (en fonction d'une id d'humain)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Message(s) trouvé(s) !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Message.class)) }),
            @ApiResponse(responseCode = "404", description = "Invalid humain id supplied",
                    content = @Content) })
    @RequestMapping(path = "/humain/{humainId}", method = RequestMethod.GET)
    public List<Message> getByHumainId(@PathVariable UUID humainId) {
        return repo.findAll().stream()
                .filter(message -> message.getAuteur().getId().equals(humainId))
                .collect(Collectors.toList());    }

    @Operation(summary = "Renvoie une liste de message en fonction de si ils ont été écrit les jours précédents")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Message(s) trouvé(s) !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Message.class))
            })
    })
    @RequestMapping(path = "/past", method = RequestMethod.GET)
    public List<Message> getByPastMessage() {
        return repo.findAll().stream()
                .filter(message -> message.getDate_publication().isBefore(LocalDate.now().atStartOfDay()))
                .collect(Collectors.toList());
    }

    @Operation(summary = "Renvoie une liste de message en fonction de si ils ont été écrit aujourd'hui")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Message(s) trouvé(s) !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Message.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Message(s) pas trouvé",
                    content = @Content) })
    @RequestMapping(path = "/today", method = RequestMethod.GET)
    public List<Message> getByTodayMessage() {
        return repo.findAll().stream()
                .filter(message -> message.getDate_publication().toLocalDate().equals(LocalDate.now()))
                .collect(Collectors.toList());    }

    @Operation(summary = "Renvoie une liste de message en fonction de la date")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Message(s) trouvé(s) !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Message.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid date supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Message(s) pas trouvé",
                    content = @Content) })
    @RequestMapping(path = "/date/{date}", method = RequestMethod.GET)
    public List<Message> getByDatePublicationMessage(@PathVariable LocalDate date) {
        return repo.findAll().stream()
                .filter(message -> message.getDate_publication().toLocalDate().isEqual(date))
                .collect(Collectors.toList());
    }

    @Operation(summary = "Crée un nouveau message")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Votre nouveau message a été crée avec succès, bravo, vous devez être un grand pas poète !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Message.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Votre message n'a pas pu être publié",
                    content = @Content) })
    @RequestMapping(path = "", method = RequestMethod.POST)
    public Message createMessage(@RequestBody NewMessageDTO _toCreate, Principal principal) throws HumainNotFoundException , ClassificationNotFoundException {
        return repo.save(
                new Message(
                        clasificationRepo.findById(_toCreate.getIdClassification()).orElseThrow(ClassificationNotFoundException::new),
                        _toCreate.getTitre(),
                        _toCreate.getTexte(),
                        humainRepo.findByIdUser(userRepo.findByUsername(principal.getName()).orElseThrow(HumainNotFoundException::new).getId()),
                        new HashSet<>(tagRepo.findAllById(_toCreate.getTags()))
                )
        );
    }

    @Operation(summary = "Supprime un message en fonction de son id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Message supprimé !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Message.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Message pas supprimé",
                    content = @Content) })
    @RequestMapping(path = "/{messageId}", method = RequestMethod.DELETE)
    public ResponseEntity<UUID> deletePost(@PathVariable UUID messageId, Principal principal) {
        Optional<Message> toDelete = repo.findById(messageId);
        if (toDelete.isPresent()) {
            repo.delete(toDelete.orElseThrow());
            return new ResponseEntity<>(messageId, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
