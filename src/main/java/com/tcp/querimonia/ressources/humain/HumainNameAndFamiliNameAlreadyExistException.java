package ressources.humain;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.NoArgsConstructor;

@Hidden
@NoArgsConstructor
public class HumainNameAndFamiliNameAlreadyExistException extends Exception{

    public HumainNameAndFamiliNameAlreadyExistException(String s) {
        super(s);
    }
}
