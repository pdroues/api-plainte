package ressources.humain.status;

import ressources.matiere.Matiere;
import jakarta.persistence.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@DiscriminatorValue("PROFESSEUR")

public class StatutProfesseur extends Status{
    @Cascade(CascadeType.ALL)
    @ManyToOne
    @JoinColumn(name = "id_matiere")
    private Matiere matiere;

    @Column(name = "est_responsable")
    private Boolean estResponsable;



}
