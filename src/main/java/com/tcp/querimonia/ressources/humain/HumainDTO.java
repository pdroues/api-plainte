package ressources.humain;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Hidden
@Getter
@Setter
@NoArgsConstructor
public class HumainDTO {
    private String nom;
    private String prenom;
}
