package ressources.humain.status;

import ressources.humain.classe.Classe;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;


@Entity()
@DiscriminatorValue("ELEVE")
public class StatutEleve extends Status {
    @ManyToOne
    @JoinColumn(name = "id_classe")
    private Classe classe;
}
