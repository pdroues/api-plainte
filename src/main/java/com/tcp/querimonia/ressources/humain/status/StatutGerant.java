package ressources.humain.status;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

@Entity()
@DiscriminatorValue("GERANT")
public class StatutGerant extends Status{
    @Column(name = "structure")
    private String structure;

    @Column(name = "statut_interne")
    private String statuInterne;
}
