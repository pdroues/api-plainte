package ressources.humain.classe;

import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Hidden
@Getter
@Setter
@Entity(name = "classe")
public class Classe {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id_classe")
    private UUID id;
    @Column(name = "anne_propotion")
    private int annePropotion;
    @Column
    private String groupe;
    @Column
    private String sousgroupe;
    @Column
    private String libelle;
    @Column
    private String parcourt;
}
