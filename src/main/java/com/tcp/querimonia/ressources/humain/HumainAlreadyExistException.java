package ressources.humain;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.NoArgsConstructor;

@Hidden
@NoArgsConstructor
public class HumainAlreadyExistException extends Exception{

    public HumainAlreadyExistException(String s) {
        super(s);
    }
}
