package ressources.humain;

import ressources.message.Message;
import ressources.user.UserInMySql;
import ressources.user.UserRepository;
import ressources.user.role.RoleRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/humain")
public class HumainController {
    private HumainRepository repo;
    private UserRepository userRepository;
    private RoleRepository roleRepository;

    @Autowired
    public HumainController(HumainRepository repo, UserRepository userRepository, RoleRepository roleRepository) {
        this.repo = repo;
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
    }

    @ExceptionHandler({ HumainNotFoundException.class })
    public ResponseEntity<String> handleHumainNotFoundException() {
        return new ResponseEntity<>("La classification n'existe pas", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ HumainAlreadyExistException.class })
    public ResponseEntity<String> HumainAlreadyExistException() {
        return new ResponseEntity<>("L'utilisateur existe déjà pour votre compte", HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler({ HumainNameAndFamiliNameAlreadyExistException.class })
    public ResponseEntity<String> HumainNameAndFamiliNameAlreadyExistException() {
        return new ResponseEntity<>("Le nom et prénom existent déjà, n'essayez pas d'usurper qqun", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ NotYourHumainException.class })
    public ResponseEntity<String> NotYourHumainException() {
        return new ResponseEntity<>(NotYourHumainException.errMess, HttpStatus.BAD_REQUEST);
    }

    @Operation(summary = "Renvoie la liste de tous les humains")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tous les humains renvoyés",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Message.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = " not found",
                    content = @Content) })
    @RequestMapping(path = "", method = RequestMethod.GET)
    public List<Humain> findAllHumans() {
        return repo.findAll();
    }

    // Renvoie un humain en fonction d'un id
    @Operation(summary = "Renvoie un humain en fonction d'un id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Humain trouvé !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Message.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Humain pas trouvé pas trouvé",
                    content = @Content) })
    @RequestMapping(path = "{id}", method = RequestMethod.GET)
    public Humain getById(@PathVariable UUID id) throws HumainNotFoundException {
        return repo.findById(id).orElseThrow(HumainNotFoundException::new);
    }

    @Operation(summary = "Crée un nouvel humain")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Votre nouvel humain a été créé avec succès, bravo !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Message.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Votre humain n'a pas pu être créé",
                    content = @Content) })
    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<Humain> postHumain(Principal principal, @RequestBody HumainDTO _toCreate) throws HumainAlreadyExistException, HumainNameAndFamiliNameAlreadyExistException {
        UserInMySql user = userRepository.findByUsername(principal.getName()).orElseThrow();
        if (repo.existsByIdUser(user.getId())) {
            throw new HumainAlreadyExistException("Votre humain lié à ce compte existe déjà.");
        }
        if (repo.existsByNomAndPrenom(_toCreate.getNom(), _toCreate.getPrenom())) {
            throw new HumainNameAndFamiliNameAlreadyExistException("Le nom et prénom existent déjà, n'essayez pas d'usurper qqun");
        }
        return new ResponseEntity<>(
            repo.save(new Humain(
                    _toCreate.getNom(),
                    _toCreate.getPrenom(),
                    user.getId()
                )
            ),
            HttpStatus.OK
        );
    }



    @Operation(summary = "Mettre à jour un humain en fonction de son id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Humain mis à jour !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Message.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Humain non trouvé",
                    content = @Content) })
    @RequestMapping(path = "{toModifyId}", method = RequestMethod.PUT)
    public ResponseEntity<Humain> putHumain(Principal principal, @PathVariable UUID toModifyId, @RequestBody HumainDTO _newHumain) throws HumainNotFoundException, NotYourHumainException {
        UserInMySql user = userRepository.findByUsername(principal.getName()).orElseThrow();
        Optional<Humain> toUpdate = repo.findById(toModifyId);

        if (toUpdate.isEmpty()) {
            throw new HumainNotFoundException();
        }

        if (!toUpdate.orElseThrow().getIdUser().equals(user.getId()) ) {
            throw new NotYourHumainException();
        }

        Humain humainToUpdate = toUpdate.orElseThrow();
        humainToUpdate.setNom(_newHumain.getNom());
        humainToUpdate.setPrenom(_newHumain.getPrenom());

        repo.save(humainToUpdate);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Supprime un humain en fonction de son id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Humain éliminé !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Message.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Humain pas éliminé",
                    content = @Content) })
    @RequestMapping(path = "{toDeleteId}", method = RequestMethod.DELETE)
    public ResponseEntity<String> delete(Principal principal, @PathVariable UUID toDeleteId) {
        UserInMySql user = userRepository.findByUsername(principal.getName()).orElseThrow();
        Optional<Humain> toDelete = repo.findById(toDeleteId);

        if (toDelete.isEmpty()) {
            return new ResponseEntity<>("Humain non trouvé", HttpStatus.NOT_FOUND);
        }
        UUID idAdmin = roleRepository.findByLibelle("ROLE_ADMIN").getId();
        if(user.getRoles().stream().anyMatch(r -> r.getId() == idAdmin)) {
            repo.delete(toDelete.orElseThrow());
            return new ResponseEntity<>("Humain N°" + toDelete.get().getId() + " supprimé, Bravo", HttpStatus.OK);

        }
        return new ResponseEntity<>("Vous ne pouvez pas suprimmer une humain qui n'est pas le votre", HttpStatus.UNAUTHORIZED);
    }
}
