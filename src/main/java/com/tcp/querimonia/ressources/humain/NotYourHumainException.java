package ressources.humain;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Hidden
@NoArgsConstructor
@Getter
public class NotYourHumainException extends Exception{
    public static String errMess = "Cet humain ne vous appartiens pas, vous ne pouvez pas appliquer ce traitement.";

    public NotYourHumainException(String s) {
        super(s);
    }
}
