package ressources.humain;

import ressources.humain.status.Status;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Hidden
@Getter
@Setter
@NoArgsConstructor
@Entity(name = "humain")
public class Humain {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id_humain")
    private UUID id;

    @Column(name = "id_user")
    private UUID idUser;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Cascade(CascadeType.ALL)
    @ManyToMany
    @JoinTable(
        name = "possede_statut",
        joinColumns = @JoinColumn(name = "id_humain"),
        inverseJoinColumns = @JoinColumn(name = "id_statut")
    )
    private Set<Status> status;

    public Humain(String nom, String prenom, UUID isUser) {
        this.nom = nom;
        this.prenom = prenom;
        this.idUser = isUser;
        this.status = new HashSet<>();
    }
}
