package ressources.humain;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.NoArgsConstructor;

@Hidden
@NoArgsConstructor
public class HumainNotFoundException extends Exception{

    public HumainNotFoundException(String s) {
        super(s);
    }
}