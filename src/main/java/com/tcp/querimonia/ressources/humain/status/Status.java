package ressources.humain.status;

import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Hidden
@Getter
@Setter
@Entity(name = "status")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="status_type",
        discriminatorType = DiscriminatorType.STRING)
public abstract class Status {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id_status")
    private UUID id;

    @Column(name = "libelle")
    private String libelle;

}
