package ressources.humain.classe;


import ressources.message.clasification.ClassificationNotFoundException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/classe")
public class ClasseController {
    @Autowired
    private ClasseRepository repo;

    @Operation(summary = "Renvoie tous les enregistrements de classes.")
    @GetMapping()
    public List<Classe> getAll() {
        return repo.findAll();
    }

    @Operation(summary = "Récupère une classe en fonction de son identifiant.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Classe trouvée !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Classe.class)) }),
            @ApiResponse(responseCode = "400", description = "Identifiant invalide fourni", content = @Content),
            @ApiResponse(responseCode = "404", description = "Classe non trouvée", content = @Content)
    })@RequestMapping(path = "{id}", method = RequestMethod.GET)
    public Classe getById(@PathVariable UUID id) {
        return repo.findById(id).orElseThrow(ClassificationNotFoundException::new);
    }

    @Operation(summary = "Récupère une liste de classes en fonction de l'année de promotion.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Classes trouvées !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Classe.class)) }),
            @ApiResponse(responseCode = "400", description = "Année de promotion invalide fournie", content = @Content)
    })
    @RequestMapping(path = "/anne/{annePromo}", method = RequestMethod.GET)
    public List<Classe> getByPromo(@PathVariable int annePromo) {
        return repo.findAll().stream().filter(classe -> classe.getAnnePropotion() == annePromo).collect(Collectors.toList());
    }
}
