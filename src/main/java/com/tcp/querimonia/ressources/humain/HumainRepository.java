package ressources.humain;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@Hidden
@RepositoryRestResource
public interface HumainRepository extends JpaRepository<Humain, UUID> {
    boolean existsByIdUser(UUID idUser);
    Humain findByIdUser(UUID idUser);
    boolean existsByNomAndPrenom(String nom, String prenom);
}
