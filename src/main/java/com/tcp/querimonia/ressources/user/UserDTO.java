package ressources.user;

import lombok.Data;

@Data
public class UserDTO {
    private String username;
    private String password;
}
