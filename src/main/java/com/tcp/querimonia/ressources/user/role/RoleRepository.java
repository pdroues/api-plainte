package ressources.user.role;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

@Hidden
public interface RoleRepository extends JpaRepository<Role, UUID> {
    Role findByLibelle(String libelle);

}
