package ressources.user.privileges;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

@Hidden
public interface PrivilegesRepository extends JpaRepository<Privilege, UUID> {
    Privilege findByName(String name);
}
