package ressources.reponse;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.NoArgsConstructor;

@Hidden
@NoArgsConstructor
public class ReponseNotFoundException extends RuntimeException{

    public ReponseNotFoundException(String s) {
        super(s);
    }
}
