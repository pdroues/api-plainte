package ressources.reponse;


import ressources.humain.HumainRepository;
import ressources.message.MessageNotFoundException;
import ressources.message.MessageRepository;
import ressources.user.UserRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/reponse")
public class ReponseController {

    private final ReponseRepository repo;
    private final UserRepository userRepository;
    private final HumainRepository humainRepository;
    private final MessageRepository messageRepository;

    @Autowired
    public ReponseController(ReponseRepository repo, UserRepository userRepository, HumainRepository humainRepository, MessageRepository messageRepository) {
        this.repo = repo;
        this.userRepository = userRepository;
        this.humainRepository = humainRepository;
        this.messageRepository = messageRepository;
    }

    @ExceptionHandler({ MessageNotFoundException.class })
    public ResponseEntity<String> handleMessageNotFoundException() {
        return new ResponseEntity<>("Impossible de créer la réponse, le message n'existe pas", HttpStatus.BAD_REQUEST);
    }

    @Operation(summary = "Renvoie toutes les réponses")
    @GetMapping()
    public List<Reponse> getAll() { return repo.findAll(); }

    @Operation(summary = "Renvoie une réponse en fonction d'un id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Réponse trouvée !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Reponse.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Réponse pas trouvée",
                    content = @Content) })
    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Reponse getById(@PathVariable UUID id) {
        return repo.findById(id).orElseThrow(ReponseNotFoundException::new);
    }

    // Renvoie toutes les réponses à un message en fonction de l'ID d'un message particulier
    @Operation(summary = "Renvoie une réponse d'un message (en fonction de son id)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Réponse trouvée !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Reponse.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Réponse pas trouvée",
                    content = @Content) })
    @RequestMapping(path = "/message/{messageId}", method = RequestMethod.GET)
    public List<Reponse> getByMessageId(@PathVariable UUID messageId) {
        return repo.findAll().stream()
                .filter(reponse -> reponse.getMessage().getId().equals(messageId))
                .collect(Collectors.toList());
    }

    // Méthode à ajouter ?
    // Renvoie un message en fonction de qui l'a écrit (en fonction d'un humain)

    // I don't like it
    @Operation(summary = "Crée une nouvelle réponse")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Votre réponse a été crée avec succès, bravo, vous devez être un grand pas poète !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Reponse.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Votre réponse n'a pas pu être publié",
                    content = @Content) })
    @RequestMapping(method = RequestMethod.POST, path = "/")
    public Reponse createReponse(@RequestBody NewReponseDTO _toCreate, Principal principal) {
        return repo.save(
            new Reponse(
                LocalDateTime.now(),
                humainRepository.findByIdUser(userRepository.findByUsername(principal.getName()).orElseThrow().getId()),
                messageRepository.findById(_toCreate.getIdMessage()).orElseThrow(MessageNotFoundException::new),
                _toCreate.getTexte()
            )
        );
    }

    @Operation(summary = "Supprime une réponse en fonction de son id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Message supprimé !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Reponse.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Message pas supprimé",
                    content = @Content) })
    @RequestMapping(path = "/{reponseId}", method = RequestMethod.DELETE)
    public ResponseEntity<UUID> deleteReponse(@PathVariable UUID reponseId) {
        Optional<Reponse> toDelete = repo.findById(reponseId);
        if (toDelete.isPresent()) {
            repo.delete(toDelete.orElseThrow());
            return new ResponseEntity<>(reponseId, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
