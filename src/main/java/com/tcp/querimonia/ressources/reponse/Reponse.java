package ressources.reponse;

import ressources.humain.Humain;
import ressources.message.Message;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Hidden
@Data
@NoArgsConstructor
@Entity(name = "reponse")
public class Reponse {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id_reponse")
    private UUID id;

    @Column(name = "date_reponse")
    private LocalDateTime date_reponse =  LocalDateTime.now();

    @ManyToOne
    @JoinColumn(name = "id_humain")
    private Humain humain;

    @ManyToOne
    @JoinColumn(name = "id_message")
    private Message message;

    @Column(name = "texte")
    private String texte;

    public Reponse(LocalDateTime date_reponse, Humain humain, Message message, String texte) {
        this.date_reponse = date_reponse;
        this.humain = humain;
        this.message = message;
        this.texte = texte;
    }
}
