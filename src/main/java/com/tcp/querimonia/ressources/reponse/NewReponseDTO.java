package ressources.reponse;

import lombok.Data;

import java.util.UUID;

@Data
public class NewReponseDTO {
    private UUID idMessage;
    private String texte;
}
