package ressources.matiere;

import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Hidden
@Getter
@Setter
@Entity(name = "matiere")
public class Matiere {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id_matiere")
    private UUID id;

    @Column(name = "code")
    private String codeMatiere;

    @Column(name = "libelle")
    private String libelle;
}
