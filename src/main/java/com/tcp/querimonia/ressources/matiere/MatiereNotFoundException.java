package ressources.matiere;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.NoArgsConstructor;

@Hidden
@NoArgsConstructor
public class MatiereNotFoundException extends RuntimeException{

    public MatiereNotFoundException(String s) {
        super(s);
    }
}