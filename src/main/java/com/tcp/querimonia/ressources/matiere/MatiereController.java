package ressources.matiere;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/matiere")
public class MatiereController {
    @Autowired
    private MatiereRepository repo;

    @Operation(summary = "Récupère la liste de toutes les matières.")
    @GetMapping()
    public List<Matiere> getAll() {
        return repo.findAll();
    }


    @ExceptionHandler({ MatiereNotFoundException.class })
    public ResponseEntity handlePersonAlreadyInGroupException() {
        return new ResponseEntity("La matière n'existe pas", HttpStatus.NOT_FOUND);
    }

    @Operation(summary = "Récupère une matière en fonction de son identifiant.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Matière trouvée !",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Matiere.class)) }),
            @ApiResponse(responseCode = "400", description = "Identifiant invalide fourni", content = @Content),
            @ApiResponse(responseCode = "404", description = "Matière non trouvée", content = @Content)
    })
    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Matiere getById(@PathVariable UUID id) {
        return repo.findById(id).orElseThrow(MatiereNotFoundException::new);
    }
}
