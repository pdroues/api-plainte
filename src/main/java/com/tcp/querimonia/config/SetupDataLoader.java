package config;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ressources.user.UserInMySql;
import ressources.user.UserRepository;
import ressources.user.privileges.Privilege;
import ressources.user.privileges.PrivilegesRepository;
import ressources.user.role.Role;
import ressources.user.role.RoleRepository;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Component
public class SetupDataLoader implements
        ApplicationListener<ContextRefreshedEvent> {

    boolean alreadySetup = false;

    @Value("${admin.username}")
    private String adminUsername;
    @Value("${admin.password}")
    private String adminPassword;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PrivilegesRepository privilegeRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (alreadySetup)
            return;
        Privilege readPrivilege
                = createPrivilegeIfNotFound("READ_PRIVILEGE");
        Privilege writePrivilege
                = createPrivilegeIfNotFound("WRITE_PRIVILEGE");
        List<Privilege> adminPrivileges = Arrays.asList(
                readPrivilege, writePrivilege);

        createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
        createRoleIfNotFound("ROLE_USER", Arrays.asList(readPrivilege));
        createRoleIfNotFound("ROLE_PROF", Arrays.asList(readPrivilege));

        Role adminRole = roleRepository.findByLibelle("ROLE_ADMIN");
        UserInMySql user = new UserInMySql();
        user.setUsername(adminUsername);
        user.setPassword(passwordEncoder.encode(this.adminPassword));
        user.setRoles(Set.of(adminRole));
        createUserIfNotFound(user);

        alreadySetup = true;
    }

    @Transactional
    public Privilege createPrivilegeIfNotFound(String name) {

        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege(name);
            privilegeRepository.save(privilege);
        }
        return privilege;
    }

    @Transactional
    public Role createRoleIfNotFound(
            String libelle, Collection<Privilege> privileges) {

        Role role = roleRepository.findByLibelle(libelle);
        if (role == null) {
            role = new Role(libelle);
            role.setPrivileges(privileges);
            roleRepository.save(role);
        }
        return role;
    }

    @Transactional
    public UserInMySql createUserIfNotFound(
            UserInMySql _toCreate) {
        if (!userRepository.existsUserByUsername(_toCreate.getUsername())) {
            return userRepository.save(_toCreate);
        }
        return null;
    }
}