
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class HelloWorldController {
    @GetMapping("/")
    public String helloWorld() {
        return "HelloWorld";
    }

    @GetMapping("user")
    public String helloWorlduser(Principal principal) {
        return "HelloWorld user : "+ principal.getName();
    }

    @GetMapping("admin")
    public String helloWorldadmin(Principal principal) {
        return "HelloWorld admin : "+ principal.getName();
    }
}
